# DEPRECATED!!!
# 
# 
# Gammalt repository som inte används längre. Flyttas till Arkiv efter beslut av Tjänstekontraktsförvaltningen (2020-03-02)
# * Instruktioner för hur man kommer igång med bitbucket http://rivta.se/bitbucket/git-sourcetree.html
# [Till wiki](https://bitbucket.org/rivta-domains/best-practice/wiki)
# [Till ärendehantering](https://bitbucket.org/rivta-domains/local.clinicalprocess.healthcond.basic/issues)
# [Till VIFO-kartan](http://www.inera.se/Documents/TJANSTER_PROJEKT/Arkitektur_och_regelverk/vifo_kartan_2013-01-01.pdf)
# [Senaste TKB]()